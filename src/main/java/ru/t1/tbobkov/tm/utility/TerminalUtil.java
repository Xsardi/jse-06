package ru.t1.tbobkov.tm.utility;

import java.util.Scanner;

public interface TerminalUtil {
    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

}
