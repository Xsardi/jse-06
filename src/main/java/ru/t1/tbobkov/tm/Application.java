package ru.t1.tbobkov.tm;

import ru.t1.tbobkov.tm.constant.ArgumentConstant;
import ru.t1.tbobkov.tm.constant.CommandConstant;
import ru.t1.tbobkov.tm.utility.TerminalUtil;

public class Application {

    public static void main(String[] args) {
        processArgs(args);
        processCommands();
    }

    private static void processArgs(final String[] args) {
        if (args == null || args.length < 1) return;
        processArg(args[0]);
        exit();
    }

    private static void processCommands() {
        System.out.println("*** TASK MANAGER ***");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.printf("\n");
            System.out.println("ENTER COMMAND: ");
            final String command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    private static void showVersion() {
        System.out.println("[version]");
        System.out.println("1.6.0");
    }

    private static void showAbout() {
        System.out.println("[about]");
        System.out.println("Name: Timur Bobkov");
        System.out.println("E-mail: tbobkov@t1-consulting.ru");
    }

    private static void showHelp() {
        System.out.println("[help]");
        System.out.printf("%s, %s - show version info\n", CommandConstant.VERSION, ArgumentConstant.VERSION);
        System.out.printf("%s, %s - show developer info\n", CommandConstant.ABOUT, ArgumentConstant.ABOUT);
        System.out.printf("%s, %s - show this list of commands\n", CommandConstant.HELP, ArgumentConstant.HELP);
        System.out.printf("%s - close application\n", CommandConstant.EXIT);
    }

    private static void showArgumentError() {
        System.out.println("[error]");
        System.out.println("incorrect argument, use '-h' to see the list of commands and arguments");
        System.exit(1);
    }

    private static void showCommandError() {
        System.out.println("[error]");
        System.out.println("incorrect command, type 'help' to see the list of commands and arguments");
    }

    private static void processArg(final String arg) {
        switch (arg.toLowerCase()) {
            case ArgumentConstant.VERSION:
                showVersion();
                break;
            case ArgumentConstant.ABOUT:
                showAbout();
                break;
            case ArgumentConstant.HELP:
                showHelp();
                break;
            default:
                showArgumentError();
                break;
        }
    }

    private static void processCommand(final String command) {
        switch (command.toLowerCase()) {
            case CommandConstant.VERSION:
                showVersion();
                break;
            case CommandConstant.ABOUT:
                showAbout();
                break;
            case CommandConstant.HELP:
                showHelp();
                break;
            case CommandConstant.EXIT:
                exit();
                break;
            default:
                showCommandError();
                break;
        }
    }

    private static void exit() {
        System.exit(0);
    }

}