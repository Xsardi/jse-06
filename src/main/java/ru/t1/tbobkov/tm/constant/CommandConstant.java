package ru.t1.tbobkov.tm.constant;

public final class CommandConstant {

    private CommandConstant() {
    }

    public static final String HELP = "help";

    public static final String VERSION = "version";

    public static final String ABOUT = "about";

    public static final String EXIT = "exit";

}
